import re
import sys
import shutil
from os import mkdir
from os.path import join, split, splitext
from collections import OrderedDict

from structlog import get_logger

from pdk.definitions import LIBS, LIBPATH


logger = get_logger(__name__)
IMPORT_REGEX = re.compile(r"--@include ([\w]+)")


def load_lib(libname):
    logger.info("loading", libname=libname)

    fpath = LIBS[libname]

    with open(join(fpath, f"{libname}.lua")) as infile:
        code = infile.readlines()

    return code


def resolve_dependencies(code, alias=None, namespace=None, seen=None, match=IMPORT_REGEX.match):
    """
    
    Args:
        code: Lib code as a list of lines
        alias: Lib alias
        namespace: Current namespace
        seen: Upstream dependencies in this branch 

    Returns:

    """
    global logger

    if namespace is None:
        namespace = OrderedDict()  # Just in case python <= 3.6.1

    if alias is None:
        alias = "root"

    if seen is None:
        seen = {alias}

    if alias in namespace:
        return

    logger = logger.bind(alias=alias)

    logger.info("resolving")
    namespace[alias] = code

    # Analize each line and resolve imports
    # all imports must be of registered libs
    for line in code:
        m = match(line)

        if m:
            libname = m.groups()[0]
            logger.info("match", libname=libname, line=line)
            libcode = load_lib(libname)

            # Detect cyclic imports within this import branch
            try:
                assert libname not in seen
            except AssertionError:
                logger.Exception("cycle detected")

            if libname not in namespace:
                seen.add(libname)

                _seen = {libname}
                _seen |= seen

                resolve_dependencies(libcode,
                                     alias=libname,
                                     namespace=namespace,
                                     seen=_seen)

                logger.info("resolved")

    return namespace


def clean_imports(code, match=IMPORT_REGEX.match):
    for line in code:
        line = line.rstrip()
        if line and not match(line):
            yield line


def resolve_code(code):
    logger.info("resolving")
    namespace = resolve_dependencies(code)

    output = []
    for alias, source in reversed(namespace.items()):
        if alias != "root":
            output.append(f"\n-- lib: {alias}\n")
        else:
            output.append(f"\n-- source\n")
        for line in clean_imports(source):
            output.append(line)
    output.append("\n") # we need this separator
    return output


def new_lib(libname):
    if libname not in LIBS:
        lib_root = join(LIBPATH, libname)
        mkdir(lib_root)
        with open(join(lib_root, f"{libname}.lua"), "w"):
            print("Creating")
    else:
        raise FileExistsError("Lib already exists")


def list_libs():
    print("Libs:")
    for lib in LIBS.keys():
        sys.stdout.write(f"  -{lib}\n")


def import_lib(lua_path):
    basepath, filename = split(lua_path)
    fname, ext = splitext(filename)

    assert fname not in LIBS, f"lib {fname} already exists"

    basedir = join(LIBPATH, fname)
    mkdir(basedir)
    shutil.copy(lua_path, basedir)

    logger.info("imported", fname=fname)
