import re
import sys
import subprocess
from os import mkdir
from collections import defaultdict
from os.path import join, split, splitext, exists

from structlog import get_logger

from pdk.base_cart import GFF, GFX, HEADER, LUA, MAP, MUSIC, SFX
from pdk.definitions import CARTSPATH, CARTS, PICOPATH
from pdk.lib import resolve_code


logger = get_logger(__name__)


## Build cartridge with default base_cart
DEFAULT_OUTPUT_CART = "/tmp/composite-cart.p8"
BASE_CARTRIDGE = "base_cart"
BASE_PARTS = {
    "gff": GFF,
    "gfx": GFX,
    "map": MAP,
    "music": MUSIC,
    "sfx": SFX,
    "lua": LUA,
    "header": HEADER,
}
PARTS = [
    "__lua__",
    "__gfx__",
    "__gff__",
    "__map__",
    "__sfx__",
    "__music__",
]


def dump_parts(parts, target, cart_root):
    for part, lines in parts.items():
        logger.info("importing", part=part, target=target)
        with open(join(cart_root, f"{target}.{part}"), "w") as outfile:
            outfile.writelines(lines)


def dump_cart(target_fpath, parts):
    with open(target_fpath, "w") as outfile:
        outfile.writelines(parts['header'])
        for key in PARTS:
            logger.info("adding", part=key)
            outfile.write(f"\n{key}\n")

            if key == "__lua__":
                outfile.write('\n'.join(parts[key.replace("_", "").strip()]))
            else:
                outfile.writelines(parts[key.replace("_", "")])


def load_parts(target_fpath):
    parts = {}
    logger.info("Extracting", target_fpath=target_fpath)
    for ext in [x.replace("_", "").strip() for x in PARTS] + ["header"]:
        with open(f"{target_fpath}.{ext}") as infile:
            parts[ext] = infile.readlines()
    logger.info("Extracted")
    return parts


def load_p8(fpath):
    with open(fpath, "r") as cartridge:
        # Improve this by writing to file instead of creating a list.
        parts = defaultdict(list)
        head = parts["header"].append

        for line in cartridge:
            if line.strip() in PARTS:  # for matching __lua__\n
                key = line.replace("_", "").strip()
                logger.info("unpacking", part=key)
                head = parts[key].append
            elif line:
                head(line)
    return parts


def create_cart(target):
    cart_root = join(CARTSPATH, target)
    mkdir(cart_root)

    return cart_root


def pack(fpath):
    _, target = split(fpath)

    target_path = join(fpath, target)
    target_fpath = join(fpath, f"{target}.p8")
    logger.info("packing", target_fpath=target_fpath)
    parts = load_parts(target_path  )
    parts['lua'] = resolve_code(parts['lua'])

    dump_cart(target_fpath, parts)
    logger.info("packed")


def unpack(fpath, target):
    logger.info("unpacking", fpath=fpath, target=target)
    try:
        parts = load_p8(fpath)
        cart_root = create_cart(target)
        dump_parts(parts, target, cart_root)
    except FileExistsError as error:
        # Ignore because of the 'force' option was passed, otherwise it fails before
        logger.warning(target=target, error=error)
        dump_parts(parts, target, join(CARTSPATH, target))
    except Exception as error:
        logger.exception(target=target, error=error)


def run_cart(cart_name):
    if PICOPATH is None:
        raise EnvironmentError("PICOPATH environment variable not set")

    cart_path = CARTS[cart_name]
    target = join(cart_path, f"{cart_name}.p8")
    logger.info("loading")

    assert exists(target), f"cart not built: {target}"

    if sys.platform == "linux":
        subprocess.check_call([PICOPATH, '-run', target])
    elif sys.platform == "darwin":
        subprocess.check_call(["open", PICOPATH, "--args", "-run", target])
    else:
        logger.exception("Unknown OS")
        raise OSError("OS unknown: f{sys.platform}")


def new_cart(cart_name):
    if cart_name not in CARTS:
        cart_root = create_cart(cart_name)
        for ext in (p.strip() for p in "gff, gfx, map, music, sfx, lua, header".split(",")):
            with open(join(cart_root, f"{cart_name}.{ext}"), "w") as outfile:
                outfile.write(BASE_PARTS[ext])
    else:
        raise FileExistsError("Cart already exists!")


def build_cart(cart_name):
    cart_path = CARTS[cart_name]
    logger.info("Building", path=cart_path)
    pack(cart_path)


def list_carts():
    print("Carts:")
    for cart in CARTS.keys():
        sys.stdout.write(f"  -{cart}\n")


def import_cart(p8_path, overwrite):
    bpath, target = split(p8_path)
    filename, ext = splitext(target)

    if filename in CARTS and not overwrite:
        raise FileExistsError(f"Cart {filename} already exists")

    unpack(p8_path, filename)
