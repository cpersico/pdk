"""
PDK Pico8 Development Kit

Usage:
  pdk run <cart_name> [-b|--build]
  pdk import cart <p8_path> [-f|--force]
  pdk import lib <lua_path>
  pdk build <cart_name>
  pdk new cart <cart_name>
  pdk new lib <lib_name>
  pdk list carts
  pdk list libs

Options:
  -h --help     Show this screen.
  --version     Show version."""


from docopt import docopt
from structlog import get_logger

from pdk.definitions import PDKHOME
import pdk.cartridge as cart
import pdk.lib as lib


logger = get_logger(__name__)


def main():
    global logger
    # We always need PDKHOME so fail if not provided
    # PICOPATH is needed just for run
    if PDKHOME is None:
        raise EnvironmentError("PDKHOME environment variable not set")

    parser = docopt(__doc__, version="0.0.1")

    if parser["<cart_name>"] is not None:
        logger = logger.bind(cart=parser["<cart_name>"])
    if parser["<lib_name>"] is not None:
        logger = logger.bind(lib=parser["<lib_name>"])

    if parser["run"]:
        if parser["-b"] or parser["--build"]:
            logger.info("pre-building")
            cart.build_cart(parser['<cart_name>'])
        logger.info("running")
        cart.run_cart(parser['<cart_name>'])

    elif parser["build"]:
        logger.info("building")
        cart.build_cart(parser['<cart_name>'])

    elif parser["new"]:
        logger.info("creating")
        if parser["cart"]:
            cart.new_cart(parser['<cart_name>'])
        elif parser["lib"]:
            lib.new_lib(parser['<lib_name>'])

    elif parser["import"]:
        if parser["<p8_path>"] is not None:
            logger = logger.bind(cart=parser["<p8_path>"])
        if parser["<lua_path>"] is not None:
            logger = logger.bind(lib=parser["<lua_path>"])

        logger.info("Importing")

        if parser["cart"]:
            overwrite = parser["-f"] or parser["--force"]
            cart.import_cart(parser['<p8_path>'], overwrite)
        elif parser["lib"]:
            lib.import_lib(parser['<lua_path>'])

    elif parser["list"]:
        logger.info("listing")
        if parser["carts"]:
            cart.list_carts()
        elif parser["libs"]:
            lib.list_libs()


if __name__ == "__main__":
    main()
