import structlog
from logging import config as logging_config

import yaml

from pdk.definitions import LOGGING_CONFIG


def initialize_logging():
    """
    Initialize the loggers from the
    YAML configuration.
    """

    structlog.configure(
        processors=[structlog.processors.JSONRenderer(indent=3, sort_keys=True)],
        context_class=structlog.threadlocal.wrap_dict(dict),
        logger_factory=structlog.stdlib.LoggerFactory(),
    )

    with open(LOGGING_CONFIG) as infile:
        config = yaml.load(infile)
        logging_config.dictConfig(config)

initialize_logging()
