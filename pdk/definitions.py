from os import getenv
from os.path import join, abspath, dirname

from pdk.utils import load_folder


PROJECT_ROOT = dirname(abspath(__file__))
PDKHOME = getenv("PDKHOME")
PICOPATH = getenv("PICOPATH")

CARTSPATH = join(PDKHOME, "carts")
LIBPATH = join(PDKHOME, "lib")

CARTS = load_folder(CARTSPATH)
LIBS = load_folder(LIBPATH)
LOGGING_CONFIG = join(PROJECT_ROOT, 'logging.yml')
