from os import listdir
from os.path import join


def load_folder(fpath: str):
    """
    Maps folder names to their root path
    """
    mapping = {cname: join(fpath, cname) for cname in listdir(fpath)}

    return mapping
