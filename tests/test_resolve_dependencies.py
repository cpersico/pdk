import pytest

from pdk.lib import resolve_dependencies
from pdk.utils import load_folder


def test_acyclic(code, mocker):
    """
    Tests the acyclic graph:
    
    A - B - D - F _
     \_ C _ D _ E _\ G
          \_ E ___/
    """

    mocker.patch("pdk.lib.LIBS", load_folder("tests/data/acyclic_import"))

    assert list(resolve_dependencies(code).keys()) == [x for x in "root,A,B,D,F,G,C,E".split(',')]


def test_cyclic(code, mocker):
    """
    Tests the cyclic graph:

    A - B - C - A
    """

    mocker.patch("pdk.lib.LIBS", load_folder("tests/data/cyclic_import"))

    with pytest.raises(Exception):
        resolve_dependencies(code)


def test_complex_cyclic(code, mocker):
    """
    Tests the cyclic graph:

      A - B - D - F - G _ 
      \_ C _ D _ E _ G  _\ A
          \_ E _ G _ A _/
    """
    mocker.patch("pdk.lib.LIBS", load_folder("tests/data/complex_cyclic_import"))

    with pytest.raises(Exception):
        resolve_dependencies(code)


def test_network(code, mocker):
    """
    Tests the graph:
        _ B _ E _    
    A /_ C _ F  _\ H
     \_ D _ G _ /
     
    Where BCD and EFG are connected in all possible combinations
    """
    mocker.patch("pdk.lib.LIBS", load_folder("tests/data/network_import"))

    assert list(resolve_dependencies(code).keys()) == [x for x in "root,A,B,E,H,F,G,C,D".split(",")]
