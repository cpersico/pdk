import pytest


@pytest.fixture
def output():
    with open("/tmp/composite_cartridge.p8") as outputfile:
        return outputfile.read()


@pytest.fixture
def code():
    _code = """@include A
    print("The code")
    """.splitlines()

    return _code
