from pdk.utils import load_folder
from pdk.lib import resolve_code


def test_code_resolve_acyclic(code, mocker):
    """
    Tests the acyclic graph:

    A - B - D - F _
     \_ C _ D _ E _\ G
          \_ E ___/
    """
    mocker.patch("pdk.lib.LIBS", load_folder("tests/data/acyclic_import"))

    resolved = '\n'.join(resolve_code(code))
    code = """
    -- lib: E
    
    print("This is E")
    
    -- lib: C
    
    print("This is C")
    
    -- lib: G
    
    print("This is G")
    
    -- lib: F
    
    print("This is F")
    
    -- lib: D
    
    print("This is D")
    
    -- lib: B
    
    print("This is B")
    
    -- lib: A
    
    print("This is A")
    
    -- source
    
    print("The code")
    
    """

    expected = '\n'.join([x.strip() for x in code.splitlines()])

    for i, (e, r) in enumerate(zip(expected.splitlines(), resolved.splitlines())):
        assert e == r, f"Line {i}"


def test_code_resolve_network(code, mocker):
    """
    Tests the graph:
        _ B _ E _    
    A /_ C _ F  _\ H
     \_ D _ G _ /
     
    Where BCD and EFG are connected in all possible combinations
    """
    mocker.patch("pdk.lib.LIBS", load_folder("tests/data/network_import"))
    resolved = '\n'.join(resolve_code(code))
    code = """
    -- lib: D

    print("This is D")

    -- lib: C

    print("This is C")

    -- lib: G

    print("This is G")

    -- lib: F

    print("This is F")

    -- lib: H

    print("This is H")

    -- lib: E

    print("This is E")

    -- lib: B

    print("This is B")

    -- lib: A

    print("This is A")

    -- source
    
    print("The code")
    
    """

    expected = '\n'.join([x.strip() for x in code.splitlines()])
    for i, (e, r) in enumerate(zip(expected.splitlines(), resolved.splitlines())):
        assert e == r, f"Line {i}"
