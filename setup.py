import sys

from setuptools import setup, find_packages


if sys.version_info < (3, 6):
    sys.exit('Sorry, Python < 3.6 is not supported')

setup(
    name='pdk',
    version="0.1",
    description="Development Kit for Pico-8",
    author="Nrai",
    author_email='nrai.eagleheart@gmail.com',
    url="www.lashistorietascomics.com",
    packages=find_packages(),
    zip_safe=False,
    test_suite='tests',
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'pdk = pdk.__main__:main',
        ]
    },
)
