# Pico-8 Development Kit

Set of tools speed up the development workflow of pico8 users.

## Features
* Library import
* Cartridge and Library Management

## Dev installation (Requires python 3.6)
* ```mkvirtualenv pdk --python=`which python3` ```
* `pip install -r requirements_dev.txt` (make sure you are in the `pdk` venv) 
* `python setup.py develop`


## Installation

####PDKHOME
Before using PDK you need to set up your `PDKHOME` directory.
This is where you will keep your cartridges and libraries.   

Your `PDKHOME` is just a directory with the following structure:

    PDKHOME
    ├── carts
    │   └── my_cart
    │       ├── my_cart.gff
    │       ├── my_cart.gfx
    │       ├── my_cart.header
    │       ├── my_cart.lua
    │       ├── my_cart.map
    │       ├── my_cart.music
    │       └── my_cart.sfx
    └── lib
        └── my_Lib
            └── my_Lib.lua
 

######You can create your own with:
    mkdir pdkhome
    cd pdkhome
    mkdir carts
    mkdir lib

Since this is completely transparent to PDK, you can put this under a VCS
such as github or bitbucket.

####Environment Variables
We need to tell PDK where your PDKHOME directory is and also
the installation path of pico-8 (PICOPATH).

###### Set the following environment variables:
    export PDKHOME="path/to/home"
    export PICOPATH="path/to/pico_folder"

You can also add them to your bashrc or zshrc file, just remember to reload the file.

######We can check that everything is working:  
    echo $PDKHOME
    echo $PICOPATH
That should print the paths you specified in the previous step.

    
## Usage
All commands are invoked through the pdk command

### run <cart_name>
    Runs cart *cart_name*
    Throws an error if the cart does not exist in the library.

### import cart <cart_name>
    Creates a cart from an existing p8 file.
    Throws an error if the cart does exists in the library.

### build <cart_name>
    Generates a p8 file from the cart

### import cart <p8_path>
    Creates a cartridge from the provided p8 filepath (use --force if
    the cartridge already exists)
    
### import lib <lua_path>
    Creates a lib from the provided lua filepath

### list carts
    List all cartridges

### list libs
    List all libs

### new_cart <cart_name>
    Creates a cart
    If the name already exists in the library
    throws an error.


### new_lib <lib_name>
    Creates a lib named lib_name
    If the name already exists in the library
    throws an error.


### Abstractions
#### Cartridge
Decomposition of a typical pico8 .p8 file.  
Each section is an inpendent file that can be modified in any  
editor. The carts are registered to the library.  
Example structure:

    cart_name/
        cart_name.gff
        cart_name.gfx
        cart_name.sfx
        cart_name.music
        cart_name.lua
        cart_name.map
        cart_name.header

Pico-8 sections (see Pico-8 API documentation for more details)
* gff: gfx bitmask
* gfx: sprites
* sfx: sound
* music: music
* lua: game code
* map: tileset map

Custom section
* header: cartridge metadata

#### Lib
A library represents a piece of code that can be 
reused in projects.  
Instead of copying every code in the same file you can write it as
a lib and include it in the code with the include notation.
PDK will include the code in the resulting p8 file when building the project.

    ie:  
    --@include lib_name
#### Carts
A folder where all cartridges are stored.
#### Libs
A folder where all libraries are stored


## Examples
First let's start by creating a new cart  
`pdk new cart my_cart`

Then create a new library  
`pdk new lib greeter`

Now using your favorite editor we are going to add some
code to our library.  
* Open the file `greeter/greeter.lua` in your `lib` directory of `PDKHOME`
* Add the following lines:
    ```
    function greet(name)
        print("hello"..name)
    end
    ```
* Save your file

Now we are going to add code to our cartridge (our actual game code)
* Open the file `my_cart/my_cart.lua` in your `carts` directory of `PDKHOME`
* Add the following lines:
```
--@include greeter

print("Hello World")
greet("pico-8")
```
* Save your file

And we are done! let's test this:
* `pdk build my_cart`
* `pdk run my_cart`

Pico 8 should launch and load my_cart automatically printing the 
lines we defined above.
