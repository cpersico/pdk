clean:
	git clean -fd build
	find . -type f -name "*.pyc" -delete
	find . -type f -name "*__pycache__" -delete
	find . -type f -name "*.orig" -delete
